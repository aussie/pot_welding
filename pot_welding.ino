#define TRIGGER_PIN 12
#define SPEAKER_PIN 11
#define WELDING_PIN 10
#define TIME_PIN A0

#define PRE_WELDING_TONE 1500
#define WELDING_TONE 1000

#define BEEP_DELAY 200

#define MIN_WELD_PULSE 10
#define MAX_WELD_PULSE 800

#define DEBOUNCE_DELAY 100

unsigned int weld_pulse = MIN_WELD_PULSE;

byte fired = 0;
double pulse_divider = 1024.0 / (MAX_WELD_PULSE - MIN_WELD_PULSE);

void setup() {
  pinMode(TRIGGER_PIN, INPUT);
  pinMode(SPEAKER_PIN, OUTPUT);
  pinMode(WELDING_PIN, OUTPUT);

  digitalWrite(TRIGGER_PIN, HIGH);
  digitalWrite(WELDING_PIN, LOW);

  Serial.begin(115200);
}

void fire(int weld_pulse) {
  tone(SPEAKER_PIN, PRE_WELDING_TONE);
  delay(BEEP_DELAY);
  noTone(SPEAKER_PIN);
  delay(BEEP_DELAY);
  
  tone(SPEAKER_PIN, PRE_WELDING_TONE);
  delay(BEEP_DELAY);
  noTone(SPEAKER_PIN);
  delay(BEEP_DELAY);
    
  tone(SPEAKER_PIN, PRE_WELDING_TONE);
  delay(BEEP_DELAY);
  noTone(SPEAKER_PIN);
  delay(BEEP_DELAY);

  // first impulse
  tone(SPEAKER_PIN, WELDING_TONE);
  digitalWrite(WELDING_PIN, HIGH);
  delay(weld_pulse);
  digitalWrite(WELDING_PIN, LOW);
  noTone(SPEAKER_PIN);
  
  // second impulse
  delay(100);
  digitalWrite(WELDING_PIN, HIGH);
  delay(weld_pulse);
  digitalWrite(WELDING_PIN, LOW);
  
  delay(500);
}

void loop() {
  int timeValue = analogRead(TIME_PIN);
   
  byte btn = digitalRead(TRIGGER_PIN);
  if(!btn) {   
    delay(DEBOUNCE_DELAY);
    
    if(btn == digitalRead(TRIGGER_PIN) && !fired) {
      weld_pulse = timeValue / pulse_divider;
      
      if(weld_pulse < MIN_WELD_PULSE) {
        weld_pulse = MIN_WELD_PULSE;
      } else
      if(weld_pulse > MAX_WELD_PULSE) {
        weld_pulse = MAX_WELD_PULSE;
      }
      
      Serial.print("Weld pulse value: ");
      Serial.println(weld_pulse);
           
      fire(weld_pulse);
      fired = 1;
    }    
  } else {
    fired = 0;
  }

  delay(10);
}
